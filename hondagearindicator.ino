// Honda Gear Indicator - Driven via DLC RPM+SPEED readout
// (C) Copyright Simon Love 2016
// Released under GPL 3.0 Licence (tbc)
// Development Mode

#include <SoftwareSerial.h>
#include <EEPROM.h>

bool dot = true;

#define DEBUG (true)

// PIN MAP : { TL, T, TR, M, BL, B, BR, D }
#define MAX_SEGMENTS (8)

// PIN MAP :                                      { TL, T, TR, M, BL, B, BR, D }
// const uint8_t pinSegmentMap[MAX_SEGMENTS] =    { 8,  7,  9, 6,  5, 4, 3,  2 };
// const uint8_t pinSegmentMap[MAX_SEGMENTS] =       { 6,  7,  8, 5,  4, 3, A3, A2 };
const uint8_t pinSegmentMap[MAX_SEGMENTS] =       { A3,  3,  4, A2,  8, 7, 6, 5 };

// pin map for DOT
#define PIN_DOT (7) 

// Predefined Macro characters
#define DIGIT_0 (0)
#define DIGIT_1 (1)
#define DIGIT_2 (2)
#define DIGIT_3 (3)
#define DIGIT_4 (4)
#define DIGIT_5 (5)
#define DIGIT_6 (6)
#define DIGIT_7 (7)
#define DIGIT_8 (8)
#define DIGIT_9 (9)

#define DIGIT_DASH (10)
#define DIGIT_E (11)
#define DIGIT_C (12)
#define DIGIT_U (13)
#define DIGIT_G (14)
#define DIGIT_O (0)
#define DIGIT_P (15)
#define DIGIT_R (15)

#define DIGIT_UNDERSCORE (16)

#define SERIALDEVICE Serial
#define SERIALTXPIN (11)
#define SERIALRXPIN (12)

#define LINCSPIN (2)

#define PACKET_BUFFER_SIZE (64)

const uint8_t digits[][MAX_SEGMENTS] = {  
// { TL, T, TR, M, BL, B, BR, D }
   { 1,  1, 1,  0, 1,  1, 1,  0 },  // 0
   { 0,  0, 1,  0, 0,  0, 1,  0 },  // 1
   { 0,  1, 1,  1, 1,  1, 0,  0 },  // 2
   { 0,  1, 1,  1, 0,  1, 1,  0 },  // 3
   { 1,  0, 1,  1, 0,  0, 1,  0 },  // 4
   { 1,  1, 0,  1, 0,  1, 1,  0 },  // 5
   { 1,  1, 0,  1, 1,  1, 1,  0 },  // 6
   { 0,  1, 1,  0, 0,  0, 1,  0 },  // 7
   { 1,  1, 1,  1, 1,  1, 1,  0 },  // 8
   { 1,  1, 1,  1, 0,  0, 1,  0 },  // 9

   { 0,  0, 0,  1, 0,  0, 0,  0 },  // Dash   
   { 1,  1, 0,  1, 1,  1, 0,  0 },  // E
   { 1,  1, 0,  0, 1,  1, 0,  0 },  // C
   { 1,  0, 1,  0, 1,  1, 1,  0 },  // U
   { 1,  1, 1,  1, 0,  1, 1,  0 },  // G

   { 1,  1, 1,  1, 1,  0, 0,  0 },  // P
   { 1,  1, 1,  1, 1,  0, 1,  0 },  // R

}; 

#define MAX_DIGITS (sizeof(digits)/MAX_SEGMENTS)

// Initialise however many samples you want to average across
uint8_t gearRatio[] = {0,0,0};
#define NUM_RATIO_SAMPLES (sizeof(gearRatio))

// http://gonzos.net/ctx/Honda-data-tables.png

uint8_t ECUinitString1[] = { 0xFE, 0x04, 0xFF, 0xFF };
// ECU: NO RESPONSE
uint8_t ECUinitString2[] = { 0x72, 0x05, 0x00, 0xF0, 0x99 };
// OUT: 0x72, 0x05, 0x00, 0xF0, 0x99
// ECU: 0x02 ,0x04, 0x00, 0xFA 
// ECU: Initialised Response, no Decode.

uint8_t ECU_BANK_11[] = { 0x72, 0x07, 0x72, 0x11, 0x00, 0x0E, 0xF6 };  
// Get as far as Speed - ignore rest of memory bank
// OUT: 0x72, 0x07, 0x72, 0x11, 0x00, 0x0E, 0xF6 
// ECU: CMD,  LEN,  CMD,  RAML, RAMH, RPMH, RPML, TPSV, TPS%, ECTV, ECTC, IATH, IATL, MAPH, MAPL, ????, ????, BAT,  MPH,  CHK     
// ECU: 0x02, 0x14, 0x72, 0x11, 0x00, 0x05, 0x5D, 0x19, 0x00, 0x2A, 0x7B, 0xAF, 0x36, 0x68, 0x45, 0xFF, 0xFF, 0x8B, 0x14, 0x18

uint8_t ECU_BANK_D1[] = { 0x72, 0x07, 0x72, 0xD1, 0x00, 0x5, 0x3F };  
// Get Gear Engaged + Engine Running
// OUT: 72, 07, 72, D1, 00, 05, 3F
// ECUL CMD,  LEN,  CMD,  RAMK, RAMH, NEUT, ????, ????, ????, ENG,  CHK
// ECU: 0x02, 0x0B, 0x72, 0xD1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xAF

bool hasRatios = false;

#define EEPROM_HEADER_BYTE (0xFF)

#define NUMBER_OF_GEARS (6)
uint16_t ECURatios[NUMBER_OF_GEARS+1] = { 100, 112, 145, 170, 192, 211, 225 };
// ECURatio = (float)SPEED/RPM * 10000
// Range observed 112-225 for Honda cbr1000rr std Gearing

SoftwareSerial mySerial(SERIALRXPIN, SERIALTXPIN); // RX, TX

uint8_t CalculateGear(uint8_t ratio) {

  // If Ratio below first geat, simply return 0
  if (ratio < ECURatios[0]) return(DIGIT_DASH);

  // loop through actual gear ratios and bail if we find out slot
  for (int i=1; i < NUMBER_OF_GEARS; i++){
   if (ratio < ((ECURatios[i] + ECURatios[i+1])/2)) return(i);
  }

  // Return Top gear if we make it this far (No upper boundary set)
  return(NUMBER_OF_GEARS);
}

// Display Digit from digitoffset array on 7 segment display
void digitDisplay( uint8_t digit ) {
   for (int segment=0; segment <= MAX_SEGMENTS; segment++) {
     int pinState = ((digits[digit][segment]) == 1) ? HIGH : LOW;
     digitalWrite(pinSegmentMap[segment], pinState);
   }
   digitDot(dot);
}

// Clear Display by setting all LCD Driving pins to LOW
void clearDisplay() {
   for (int segment=0; segment <= MAX_SEGMENTS; segment++) {
     digitalWrite(pinSegmentMap[segment], LOW);
   }
}

void digitDot( bool showDot){
     int pinState = (showDot) ? HIGH : LOW;
     digitalWrite(pinSegmentMap[PIN_DOT], pinState);
}

// Enable Honda ECU bus - ensure Serial not enabled !
void ECUinitBusAndTransceiver() {

  pinMode(1, OUTPUT);
  digitalWrite(1, LOW);
  delay(70);
  digitalWrite(1, HIGH);
  delay(120);  

  // No way to check if it worked !!!
}

void ECUsendBytes( uint8_t *byteBuffer, uint8_t bufferLen )
{for (int i=0;i<bufferLen;i++)
  {
    SERIALDEVICE.write(*(byteBuffer+i));
  }
  SERIALDEVICE.flush();

  // Dump everything we sent and the reply to HardwareSerial for debugging
  // Give ECU time to reply

  // mySerial.write(0xff);
  delay(25); 
  // mySerial.write(0xff);
  debugSerialInput();
  // mySerial.write('\n');

}

void debugSerialInput()
{
  // Dump everything we sent plus any deply in buffer
  while (SERIALDEVICE.available() > 0) {
      uint8_t val = SERIALDEVICE.read();

      if (val<16) mySerial.write('0');  // add leading 0 for single digit bytes
      mySerial.print(val,HEX);

      if (SERIALDEVICE.available() > 0) mySerial.print(", ");
  }
  mySerial.write('\n');
}

void ECUsendInitSequence() {

  //   Send first init string
  ECUsendBytes( ECUinitString1, sizeof(ECUinitString1)); 
  ECUsendBytes( ECUinitString2, sizeof(ECUinitString2)); 
}


uint8_t getAveragedGearRatioValue(){

  uint16_t average=0;

  for (int i=0;i<NUM_RATIO_SAMPLES;i++) {
      average += gearRatio[i];
  }
  return (average / NUM_RATIO_SAMPLES);
}


void storeGearRatio( uint16_t mph, uint16_t rpm ){

static uint8_t gearPtr = 0;

  if ((rpm > 0) && (mph > 5))
  {
    // Add check for invalid ratios, then add previous one maybe ???
    gearRatio[gearPtr++] = (uint8_t) (((float)mph / (float)rpm) * 10000);
    // Cycle ptr around buffer
    gearPtr = gearPtr % NUM_RATIO_SAMPLES;
  }
}

bool ECUcheckInGear() {
  delay(20);
  return true;
}


void ECUgetData() {

  uint8_t mph = 25;
  uint16_t rpm = 1350;
  uint8_t bufferCount=0;
  uint8_t ptr = 0;
  uint8_t chk = 0;
  uint8_t calcChk = 0;

  uint8_t buffer[PACKET_BUFFER_SIZE];

  // Poll ECU Data and update ratio

  for (int i=0; i< sizeof(ECU_BANK_11);i++ ) {
    SERIALDEVICE.write(ECU_BANK_11[i]);
  }
  SERIALDEVICE.flush();

  // Allow time for ECU to respond
  delay(35);

  // Populate buffer until we have read all data
  while ( (SERIALDEVICE.available() > 0 ) && ( bufferCount < PACKET_BUFFER_SIZE)) {
    // Read local echo from TX and throw away - should really check !!!
    buffer[bufferCount++] = SERIALDEVICE.read();
  }

  chk = buffer[bufferCount-1]; // Checksum = last byte read
  ptr = sizeof(ECU_BANK_11);

  // Work on principle full packet of data will be available in RX Buffer
  // Expecting a response with 0x14 bytes of payload data to checksum
  if ((buffer[ptr] == 0x02) && (buffer[ptr+1] == 0x14) && (buffer[ptr+3] == 0x11) ) {
    // mySerial.print("             ");
    for ( int i=0; i < bufferCount-1; i++)
    {
      calcChk += buffer[i];
    }
  }
  calcChk = (uint8_t)(calcChk ^ 0xFF) +1;

  // Checksum match ?
  if (calcChk != chk)  return;

  toggleDot();
  
  // Pull data from ECU Response Buffer
  mph = buffer[25];  // ECU Response point 19 (ptr=1) 
  rpm = (uint16_t) (buffer[ptr+5] << 8) + buffer[ptr+6];

  storeGearRatio(mph, rpm);
}

void loop() {

  static bool inGear = false;
  static uint16_t count=0;

  uint8_t ratio=0;

  // Keep checking until we're in gear ( Dash displays '-' from setup )
  while (!inGear) {
    inGear = ECUcheckInGear();
  }

  // Won't get here unless its been put in gear and we have gear
  while (inGear) {
    // clearDisplay();
    // Interogate ECU for Speed, RPM etc
    ECUgetData();

    ratio = getAveragedGearRatioValue();

    // Display current Gear on Dash
    digitDisplay(CalculateGear(ratio));

    // Check if we're still in gear every n iterations
    if (count++ % 10) {
        inGear = ECUcheckInGear();
    }
  }
}

bool EEPROMhasData(){
  // EEPROM byte at address 0 = 1 if data present
  return(EEPROM.read(0) == EEPROM_HEADER_BYTE);
}

// Toddle Dash Digit on and off
void flashDashDigit() {
  digitDisplay(DIGIT_DASH);
  delay(100);
  clearDisplay();
  delay(100);
}

void toggleDot() {

  digitDot(dot);
  dot = !dot;
}

void displayWelcomeString() {

  digitDisplay(DIGIT_E);
  delay(250);
  digitDisplay(DIGIT_C);
  delay(250);
  digitDisplay(DIGIT_U);
  delay(250);
  toggleDot();
  delay(250);
  toggleDot();
  delay(250);
  toggleDot();
  delay(250);
  toggleDot();
  delay(250);
  clearDisplay();
}

void displayProgString() {

  digitDisplay(DIGIT_P);
  delay(250);
  digitDisplay(DIGIT_R);
  delay(250);
  digitDisplay(DIGIT_O);
  delay(250);
  digitDisplay(DIGIT_G);
  delay(250);
  clearDisplay();
}

// Setup Function
// 
// * Hardare Init
// * EEPROM Data Load
// * Trigger Learning Mode if no Ratio Data Stored (Blink '-')
// * Initialise USART + SoftSerial (Debug Output)
// * Initialise K-Line Bus (Honda ECU)
// * Initialise ECU (No Checks on good response !!!)
// * Display '-' in prep for ECU polling Gear Indication

void setup() {


  // pinMode(pinSegmentMap[PIN_DOT],OUTPUT);

  // while (true) {
  //   toggleDot();
  //   delay(200);    
  // }

  pinMode(LINCSPIN, OUTPUT);
  pinMode(SERIALTXPIN, OUTPUT);

  // Enable Lin Transceiver (plenty of time to initiliase)
  digitalWrite(LINCSPIN, HIGH);

  mySerial.begin(38400);

  // Process Pins defined in PinSegmentMap and make them all outputs    
  for (int i=0; i <= MAX_SEGMENTS; i++)
  {
    pinMode(pinSegmentMap[i], OUTPUT);
  } 


  #ifdef DEBUG 
    EEPROM.write(0,EEPROM_HEADER_BYTE);  // Write 1 to addr - to indicate config present

    for (int i=0;i< NUMBER_OF_GEARS+1;i++)
      EEPROM.update(i+1,gearRatio[i]);  // write min ratio + 6 gear ratios to eeprom
  #endif

  if ( EEPROMhasData()) {
    for (int i=0;i< NUMBER_OF_GEARS+1;i++){
      gearRatio[i] = EEPROM.read(i+1);
    }
  } else {
    while (true) 
    {
      displayProgString();
      toggleDot();
      delay(500);
    }
  }

  displayWelcomeString();

  ECUinitBusAndTransceiver(); // Initialise ECU Bus
  SERIALDEVICE.begin(10400);  // Enable Serial Write (Idles High)
  ECUsendInitSequence();      // Send Honda ECU Init Sequence (#Todo: Add Check)
}
